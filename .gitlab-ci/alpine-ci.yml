#
# THIS FILE IS GENERATED, DO NOT EDIT
#

################################################################################
#
# Alpine checks
#
################################################################################

include:
  - local: '/templates/alpine.yml'


stages:
  - alpine_container_build
  - alpine_check


#
# Common variable definitions
#
.ci-commons-alpine:
  image: $CI_REGISTRY_IMAGE/buildah:2021-03-18.0
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_DISTRIBUTION_EXEC: 'sh test/script.sh'
    FDO_EXPIRES_AFTER: '1h'

.ci-commons-alpine@x86_64:
  extends: .ci-commons-alpine
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-x86_64-$CI_PIPELINE_ID

.ci-commons-alpine@aarch64:
  extends: .ci-commons-alpine
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID

#
# A few templates to avoid writing the image and stage in each job
#
.alpine:ci@container-build@x86_64:
  extends:
    - .fdo.container-build@alpine
    - .ci-commons-alpine@x86_64
  stage: alpine_container_build


.alpine:ci@container-build@aarch64:
  extends:
    - .fdo.container-build@alpine
    - .ci-commons-alpine@aarch64
  tags:
    - aarch64
  stage: alpine_container_build


#
# generic alpine checks
#
.alpine@check@x86_64:
  extends:
    - .ci-commons-alpine@x86_64
    - .fdo.distribution-image@alpine
  stage: alpine_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $FDO_DISTRIBUTION_EXEC properly run ;
      else
        exit 1 ;
      fi


#
# straight alpine build and test
#
alpine:latest@container-build@x86_64:
  extends: .alpine:ci@container-build@x86_64


alpine:latest@check@x86_64:
  extends: .alpine@check@x86_64
  needs:
    - alpine:latest@container-build@x86_64

# Test FDO_BASE_IMAGE. We don't need to do much here, if our
# FDO_DISTRIBUTION_EXEC script can run curl+wget this means we're running on
# the desired base image. That's good enough.
alpine:latest@base-image@x86_64:
  extends: alpine:latest@container-build@x86_64
  stage: alpine_check
  variables:
    # We need to duplicate FDO_DISTRIBUTION_TAG here, gitlab doesn't allow nested expansion
    FDO_BASE_IMAGE: registry.freedesktop.org/$CI_PROJECT_PATH/alpine/latest:fdo-ci-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: ''
    FDO_DISTRIBUTION_EXEC: 'test/test-wget-curl.sh'
    FDO_FORCE_REBUILD: 1
    FDO_DISTRIBUTION_TAG: fdo-ci-baseimage-x86_64-$CI_PIPELINE_ID
  needs:
    - alpine:latest@container-build@x86_64

#
# /cache alpine check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
alpine@cache-container-build@x86_64:
  extends: .alpine:ci@container-build@x86_64
  before_script:
      # The template normally symlinks the /cache
      # folder, but we want a fresh new one for the
      # tests.
    - mkdir runner_cache_$CI_PIPELINE_ID
    - uname -a | tee runner_cache_$CI_PIPELINE_ID/foo-$CI_PIPELINE_ID

  artifacts:
    paths:
      - runner_cache_$CI_PIPELINE_ID/*
    expire_in: 1 week

  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-cache-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_EXEC: 'bash test/test_cache.sh $CI_PIPELINE_ID'
    FDO_CACHE_DIR: $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1

#
# /cache alpine check (in check stage)
#
alpine@cache-check@x86_64:
  stage: alpine_check
  image: alpine:latest
  script:
    # in the previous stage (alpine@cache-container-build@x86_64),
    # test/test_cache.sh checked for the existance of `/cache/foo-$CI_PIPELINE_ID`
    # and if it found it, it wrote `/cache/bar-$CI_PIPELINE_ID`.
    #
    # So if we have in the artifacts `bar-$CI_PIPELINE_ID`, that means
    # 2 things:
    # - /cache was properly mounted while building the container
    # - the $FDO_CACHE_DIR has been properly written from within the
    #   building container, meaning the /cache folder has been successfully
    #   updated.
    - if [ -e $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID/bar-$CI_PIPELINE_ID ] ;
      then
        echo Successfully read/wrote the cache folder, all good ;
      else
        echo FAILURE while retrieving the previous artifacts ;
        exit 1 ;
      fi
  needs:
    - job: alpine@cache-container-build@x86_64
      artifacts: true


alpine:latest@container-build@aarch64:
  extends: .alpine:ci@container-build@aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID


alpine:latest@check@aarch64:
  extends: .alpine@check@x86_64
  tags:
    - aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID
  needs:
    - alpine:latest@container-build@aarch64


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
do not rebuild alpine:latest@container-build@x86_64:
  extends: .alpine:ci@container-build@x86_64
  stage: alpine_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - alpine:latest@container-build@x86_64


#
# check if the labels were correctly applied
#
check labels alpine@x86_64:latest:
  extends:
    - alpine:latest@check@x86_64
  image: $CI_REGISTRY_IMAGE/buildah:2021-03-18.0
  script:
    # FDO_DISTRIBUTION_IMAGE still has indirections
    - DISTRO_IMAGE=$(eval echo ${FDO_DISTRIBUTION_IMAGE})

    # retrieve the infos from the registry (once)
    - JSON_IMAGE=$(skopeo inspect docker://$DISTRO_IMAGE)

    # parse all the labels we care about
    - IMAGE_PIPELINE_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.pipeline_id"]')
    - IMAGE_JOB_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.job_id"]')
    - IMAGE_PROJECT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.project"]')
    - IMAGE_COMMIT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.commit"]')

    # some debug information
    - echo $JSON_IMAGE
    - echo $IMAGE_PIPELINE_ID $CI_PIPELINE_ID
    - echo $IMAGE_JOB_ID
    - echo $IMAGE_PROJECT $CI_PROJECT_PATH
    - echo $IMAGE_COMMIT $CI_COMMIT_SHA

    # ensure the labels are correct (we are on the same pipeline)
    - '[[ x"$IMAGE_PIPELINE_ID" == x"$CI_PIPELINE_ID" ]]'
    - '[[ x"$IMAGE_JOB_ID" != x"" ]]' # we don't know the job ID, but it must be set
    - '[[ x"$IMAGE_PROJECT" == x"$CI_PROJECT_PATH" ]]'
    - '[[ x"$IMAGE_COMMIT" == x"$CI_COMMIT_SHA" ]]'
  needs:
    - alpine:latest@container-build@x86_64


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where FDO_REPO_SUFFIX == ci_templates_test_upstream
#
pull upstream alpine:latest@container-build@x86_64:
  extends: .alpine:ci@container-build@x86_64
  stage: alpine_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_REPO_SUFFIX: alpine/ci_templates_test_upstream
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - alpine:latest@container-build@x86_64
